const ROOT_API = `${process.env.ROOT_API}`
const APP = `pr0ducts`
const ENDPOINT = `${ROOT_API}/${APP}/`

export default {
  data () {
    return {
      pr0ducts: [],
      pr0duct: undefined,
      paginationPr0duct: {
        next: undefined,
        previous: undefined,
        count: 0
      },
      queryPr0duct: {
        page: 1
      }
    }
  },
  methods: {
    listPr0duct () {
      return this.axios.get(ENDPOINT, {params: this.queryPr0duct})
        .then((response) => {
          this.pr0ducts = response.data.results
          this.paginationPr0duct.next = response.data.next
          this.paginationPr0duct.previous = response.data.previous
          this.paginationPr0duct.count = response.data.count
        })
    },
    retrievePr0duct (id) {
      return this.axios.get(`${ENDPOINT}${id}/`)
        .then((response) => {
          this.pr0duct = response.data
        })
    },
    updatePr0duct (id, payload) {
      return this.axios.patch(`${ENDPOINT}${id}/`, payload)
        .then((response) => {
          this.pr0duct = response.data
        })
    },
    createPr0duct (payload) {
      return this.axios.post(ENDPOINT, payload)
        .then((response) => {
          this.pr0duct = response.data
        })
    },
    destroyPr0duct (id) {
      return this.axios.delete(`${ENDPOINT}${id}/`)
        .then((response) => {
          this.pr0duct = undefined
        })
    },
    exportPr0duct (prefix) {
      const filename = `${prefix}_${new Date()}_.csv`
      return this.axios({
        url: `${ENDPOINT}export/`,
        method: 'GET',
        responseType: 'blob',
        params: this.queryPr0duct
      })
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response.data]))
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('download', filename)
          document.body.appendChild(link)
          link.click()
        })
    }
  }
}
